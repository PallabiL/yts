import { keyboard } from "@testing-library/user-event/dist/keyboard";
import axios from "axios" 
import { BaseURL } from "../config/baseURL"

// axios.defaults.baseURL=BaseURL;
export const APIGetTopMovies=()=>{
    // API banako
    // BaseURL+'list_movies.json' yo API ko func lai hit garni bittikai movie haruko data auxa
   return axios.get(BaseURL+'list_movies.json');
}
export const APIGetSearchedMovie=(keyword)=>{
    //  axios.get(BaseURL+'list_movies.json?query_term='+keyword); this is API
    // query_term ma j keyword rakhyo tesko value auni vayo
    return axios.get(BaseURL+'list_movies.json?query_term='+keyword);
}