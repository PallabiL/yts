// to use jsx which will get transformed into React.createElement
// to interpret html as jsx
import React from 'react';
// to do js updation and manipulation inside react 
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import {BrowserRouter} from "react-router-dom"
// render is a method which displays and takes three parameters i.e. what to display,where to display(in index.html whose id is root) and callback
ReactDOM.render(
  <React.StrictMode>
  {/* npm install react-router-dom */}
  {/* it is the component that does all the logic of disaplaying various componentns that we provide it with */}
  <BrowserRouter>
  <App />
  </BrowserRouter>
   
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
