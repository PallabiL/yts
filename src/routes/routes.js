import {Landing} from "../pages/Landing"
import {MovieDetails} from "../pages/MovieDetails"
import {Search} from "../pages/Search"
export const ROUTES=[
    // kun pathma jada kun route ma jaani
    {path:"/",component:<Landing/>,exact:true},
    // id means queryparameter
    {path:"/movie/:id",component:<MovieDetails/>,exact:true},
    {path:"/search/:keyword",component:<Search/>,exact:true}
]
