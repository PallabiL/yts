import axios from "axios";
import { useEffect, useState } from "react"
import { APIGetTopMovies } from "../API/Movie"
import { Loader } from "../common/Loader/Loader";
import { MovieCard } from "../common/MovieCard"
// subash dai
// export const Landing=()=>{
//     const [movies,setMovies]= useState([])
//     useEffect(()=>{
//         // aync -makes a function, return a  promise
//         callMovieAPI();
        
//         // await le euta kaam nasakepani arko start garxa , makes a function wait for a promise
//     },[])

//     const callMovieAPI =async() => {
//         console.log("he")
//         await axios.get('https://yts.mx/api/v2/list_movies.json').then((res)=>{
//             let movieslist  = res.data.data.movies;
//             console.log(res, res.data.data);
//             setMovies([...res.data.data.movies]);
//         });
//         console.log(movies)
//     };
//     return <section className={"wrapper"}>
//     <h1>movie title</h1>
//     {/* get,post,put,patch,delete */}
//     {/* get=kunai pani thau bata data paunalai
//         post=backenf ma gaera set garna
//         put=edit garna
//         patch=editing
//         delete=delete garna */}
//     <div className="movie-content">
//         {movies.map((movie,key)=>
//             <MovieCard key={key} movie={movie}/>)
//        }
       
//     </div>
//     </section>
// }

export const Landing=()=>{
    const [movies,setMovies]= useState([]);
    const [loading,setLoading]=useState(true);
    useEffect(async()=>{
        // axios garera back end ma hit gareko
    //     axios.get('https://yts.mx/api/v2/').then((res)=>{
    //         setMovies([...res.data.data['movies']]);
    // });

    
    // to load movies loadMovies function is created
    // application load huni bittikai loadmovies call hunxa
    await loadMovies();
},[]);
const loadMovies=async()=>{
    // loadmovies lai APIGetTopMovies()) bata tanxa
    setLoading(true);
    // backend bata ako data res ma store
    let res =  await APIGetTopMovies();
    console.log(res.data.data.movies)
    // res.data.data.movies ma data xa
    // data auni bittikai local variable ma lagera store gareko
    setMovies([...res.data.data['movies']]);
    setLoading(false)
}
    return <section className="wrapper">
        <h1>Movies List</h1>
        <div className="movie-content">
        {/* movie load hunjel loader dekhairakhxa load vaepaxi movies ko card dekhaucxa */}
          {/* map use garera harek individual oject lai movie card ma pass gareko */}
          {/* load huni bittikai value front end ma display hunxa */}
        {movies.length===0?<Loader/>:
        movies.map((movie,key)=>
            <MovieCard key={key} movie={movie}/>)
                    
        }
        </div>
    </section>
}
// package lock delete garni
// git fetch
// git checkout -b app/yts
// git pull